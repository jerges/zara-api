package com.zara.test.api.repository.domain;

import java.util.Collections;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BrandTest {

  @Test
  public void equalsVerifier() {
    EqualsVerifier.forClass(Brand.class)
        .withPrefabValues(Brand.class, new Brand(1L, "ZARA", Collections.singletonList(Price.builder().id(1L).build())),
            new Brand(1L, "ZARA", Collections.singletonList(Price.builder().id(2L).build())))
        .suppress(Warning.ANNOTATION)
        .withRedefinedSuperclass().verify();

  }

}
