package com.zara.test.api.repository.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PriceTest {

  @Test
  public void equalsVerifier() {
    EqualsVerifier.forClass(Price.class)
        .withPrefabValues(Price.class, Price.builder().brandId(new Brand(1L, "ZARA", null)).build(),
            Price.builder().brandId(new Brand(2L, "MANGO", null)).build())
        .suppress(Warning.ANNOTATION)
        .suppress(Warning.NONFINAL_FIELDS)
        .withRedefinedSuperclass().verify();
  }

}
