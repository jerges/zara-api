package com.zara.test.api.objectmother;

import com.zara.test.api.dto.PriceResponse;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public final class PriceResponseMother {

  public static PriceResponse withAllProperties() {
    return PriceResponse.builder()
        .price(BigDecimal.TEN)
        .priceList(4L)
        .brandId(1L)
        .productId(35455L)
        .startDate(LocalDateTime.MIN)
        .build();
  }

}
