package com.zara.test.api.objectmother;

import com.zara.test.api.repository.domain.Brand;
import java.util.Collections;

public class BrandMother {

  public static Brand withAllProperties() {
    return new Brand(1L, "ZARA", Collections.singletonList(PriceMother.withAllProperties()));
  }

  public static Brand withOutPrice() {
    return new Brand(1L, "ZARA", null);
  }

}
