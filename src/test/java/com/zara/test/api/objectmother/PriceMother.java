package com.zara.test.api.objectmother;

import com.zara.test.api.repository.domain.Price;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class PriceMother {

  public static Price withAllProperties() {
    return Price.builder()
        .productId(35455L)
        .brandId(BrandMother.withOutPrice())
        .priceList(4L)
        .priority(2)
        .startDate(LocalDateTime.MIN.toInstant(ZoneOffset.UTC))
        .value(BigDecimal.TEN)
        .id(1L)
        .build();
  }

}
