package com.zara.test.api.api;

import com.zara.test.api.config.JacksonConfiguration;
import com.zara.test.api.exception.ControllerAdvisor;
import java.util.List;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@Import({AopAutoConfiguration.class})
public class WebMvcTestConfig implements WebMvcConfigurer {

  @Override
  public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
    converters.add(this.jackson2HttpMessageConverter());
  }

  @Bean
  public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
    final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();
    final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(jacksonConfiguration.createObjectMapper());
    return converter;
  }

  @Bean
  public ControllerAdvisor controllerExceptionHandler() {
    return new ControllerAdvisor();
  }

}
