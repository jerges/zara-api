package com.zara.test.api.api.contract;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.zara.test.api.api.WebMvcTestConfig;
import com.zara.test.api.controller.impl.PriceControllerImpl;
import com.zara.test.api.exception.NotFoundException;
import com.zara.test.api.objectmother.PriceResponseMother;
import com.zara.test.api.service.BrandService;
import com.zara.test.api.service.PriceService;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {PriceControllerImpl.class})
@Import({WebMvcTestConfig.class})
public class PriceControllerImplContractTest {

  private static final String BASE_URL = "/price";
  private static final long BRAND_ID = 1;
  private static final long PRODUCT_ID = 35455;

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PriceService priceService;

  @MockBean
  private BrandService brandService;

  private static final LocalDateTime START_DATE_TIME = LocalDateTime.of(2021, Month.JULY, 14, 10, 0);

  @Test
  @DisplayName("HTTP-GET: find price by brand, product and start date - http-code is 200.")
  public void findPriceByBrand() throws Exception {
    final String responseJSON = this.readFileAsString("src/test/resources/response-price.json");
    when(this.priceService.getPriceByBrand(BRAND_ID, PRODUCT_ID, START_DATE_TIME))
        .thenReturn(PriceResponseMother.withAllProperties());
    this.mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{brandId}/{productId}", BRAND_ID, PRODUCT_ID)
        .param("startDate", START_DATE_TIME.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(responseJSON));
  }

  @Test
  @DisplayName("HTTP-GET: not find price by brand, product and start date - http-code is 404.")
  public void findPriceByBrandNotFound() throws Exception {
    when(this.priceService.getPriceByBrand(BRAND_ID, PRODUCT_ID, START_DATE_TIME))
        .thenThrow(new NotFoundException("El precio no existe"));
    this.mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{brandId}/{productId}", BRAND_ID, PRODUCT_ID)
        .param("startDate", START_DATE_TIME.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)))
        .andDo(print())
        .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("HTTP-GET: bad request to find price by brand, product and start date - http-code is 400.")
  public void findPriceByBrandBadRequest() throws Exception {
    this.mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL + "/{brandId}/{productId}", BRAND_ID, PRODUCT_ID))
        .andDo(print())
        .andExpect(status().isBadRequest());
  }


  private String readFileAsString(final String filePath) throws IOException {
    final StringBuilder fileData = new StringBuilder();
    final BufferedReader reader = new BufferedReader(
        new FileReader(filePath));
    final char[] buf = new char[1024];
    int numRead = 0;
    while ((numRead = reader.read(buf)) != -1) {
      final String readData = String.valueOf(buf, 0, numRead);
      fileData.append(readData);
    }
    reader.close();
    return fileData.toString();
  }

}
