package com.zara.test.api.api.end2end;

import com.zara.test.api.ApiApplication;
import com.zara.test.api.api.WebMvcTestConfig;
import com.zara.test.api.controller.PriceController;
import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.repository.domain.Brand;
import com.zara.test.api.repository.domain.Price;
import com.zara.test.api.service.BrandService;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiApplication.class})
@Import(WebMvcTestConfig.class)
public class PriceControllerIntegrationTest {

  @Autowired
  private PriceController priceController;

  @Autowired
  private BrandService service;

  @Before
  public void setUp() {
    final List<Price> priceList = new ArrayList<>();
    final Brand brand = new Brand(1L, "ZARA", null);
    final Random random = new Random();
    final Price price = Price.builder()
        .currency("EUR")
        .productId(35455L)
        .build();
    for (int i = 1; i <= 5; i++) {
      priceList.add(price.toBuilder()
          .value(BigDecimal.valueOf(random.nextDouble()))
          .brandId(brand)
          .priority(random.nextBoolean() ? 1 : 0)
          .priceList((long) i)
          .build());
    }
    priceList.get(0).setStartDate(LocalDateTime.of(2021, Month.JULY, 14, 10, 0).toInstant(ZoneOffset.UTC));
    priceList.get(1).setStartDate(LocalDateTime.of(2021, Month.JULY, 14, 16, 0).toInstant(ZoneOffset.UTC));
    priceList.get(2).setStartDate(LocalDateTime.of(2021, Month.JULY, 14, 21, 0).toInstant(ZoneOffset.UTC));
    priceList.get(3).setStartDate(LocalDateTime.of(2021, Month.JULY, 15, 10, 0).toInstant(ZoneOffset.UTC));
    priceList.get(4).setStartDate(LocalDateTime.of(2021, Month.JULY, 16, 21, 0).toInstant(ZoneOffset.UTC));
    brand.setPriceList(priceList);
    this.service.save(brand);
  }

  @Test
  @DisplayName("Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
  public void findPriceForProductD14_H10() {
    final ResponseEntity<PriceResponse> priceResponse = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.of(2021, Month.JULY, 14, 10, 0));
    Assert.assertNotNull(priceResponse.getBody());
    Assert.assertEquals(LocalDateTime.of(2021, Month.JULY, 14, 10, 0), priceResponse.getBody().getStartDate());
  }

  @Test
  @DisplayName("Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
  public void findPriceForProductD14_H16() {
    final ResponseEntity<PriceResponse> priceResponse = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.of(2021, Month.JULY, 14, 16, 0));
    Assert.assertNotNull(priceResponse.getBody());
    Assert.assertEquals(LocalDateTime.of(2021, Month.JULY, 14, 16, 0), priceResponse.getBody().getStartDate());
  }

  @Test
  @DisplayName("Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
  public void findPriceForProductD14_H21() {
    final ResponseEntity<PriceResponse> priceResponse = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.of(2021, Month.JULY, 14, 21, 0));
    Assert.assertNotNull(priceResponse.getBody());
    Assert.assertEquals(LocalDateTime.of(2021, Month.JULY, 14, 21, 0), priceResponse.getBody().getStartDate());
  }

  @Test
  @DisplayName("Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)")
  public void findPriceForProductD15_H10() {
    final ResponseEntity<PriceResponse> priceResponse = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.of(2021, Month.JULY, 15, 10, 0));
    Assert.assertNotNull(priceResponse.getBody());
    Assert.assertEquals(LocalDateTime.of(2021, Month.JULY, 15, 10, 0), priceResponse.getBody().getStartDate());
  }

  @Test
  @DisplayName("Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)")
  public void findPriceForProductD16_H21() {
    final ResponseEntity<PriceResponse> priceResponse = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.of(2021, Month.JULY, 16, 21, 0));
    Assert.assertNotNull(priceResponse.getBody());
    Assert.assertEquals(LocalDateTime.of(2021, Month.JULY, 16, 21, 0), priceResponse.getBody().getStartDate());
  }


}
