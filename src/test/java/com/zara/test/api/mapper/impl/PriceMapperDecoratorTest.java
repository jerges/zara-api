package com.zara.test.api.mapper.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.objectmother.PriceMother;
import com.zara.test.api.objectmother.PriceResponseMother;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PriceMapperDecoratorTest {


  @InjectMocks
  private PriceMapperDecoratorImpl priceMapper;

  @Test
  public void map() {
    final var price = PriceMother.withAllProperties();
    final var expected = PriceResponseMother.withAllProperties();
    final PriceResponse result = this.priceMapper.map(price);
    assertEquals(expected.getStartDate(), result.getStartDate());
    assertEquals(expected.getPrice(), result.getPrice());
    assertEquals(expected.getProductId(), result.getProductId());
    assertEquals(expected.getPriceList(), result.getPriceList());
    assertEquals(expected.getBrandId(), result.getBrandId());
  }

  @Test
  public void mapWhenBrandIsNull() {
    final var price = PriceMother.withAllProperties();
    price.setBrandId(null);
    final var expected = PriceResponseMother.withAllProperties();
    final PriceResponse result = this.priceMapper.map(price);
    assertEquals(expected.getStartDate(), result.getStartDate());
    assertEquals(expected.getPrice(), result.getPrice());
    assertEquals(expected.getProductId(), result.getProductId());
    assertEquals(expected.getPriceList(), result.getPriceList());
    assertNull(result.getBrandId());
  }
}
