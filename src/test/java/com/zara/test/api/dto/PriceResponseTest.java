package com.zara.test.api.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.zara.test.api.config.JacksonConfiguration;
import com.zara.test.api.objectmother.PriceResponseMother;
import java.io.IOException;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.json.JacksonTester;

@RunWith(MockitoJUnitRunner.class)
public class PriceResponseTest {

  private JacksonTester<PriceResponse> json;


  private static final String JSON_RESPONSE_EXAMPLE = "{\"product_id\":35455,\"brand_id\":1,\"price_list\":4,"
      + "\"start_date\":\"-999999999-01-01T00:00:00\",\"price\":10}";

  @Before
  public void setUp() {
    final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();
    JacksonTester.initFields(this, jacksonConfiguration.createObjectMapper());
  }

  @Test
  public void equalsVerifier() {
    EqualsVerifier.forClass(PriceResponse.class)
        .suppress(Warning.ANNOTATION)
        .withRedefinedSuperclass().verify();
  }


  @Test
  public void shouldDeserializeProperty() throws IOException {
    final PriceResponse dto = this.json.parse(JSON_RESPONSE_EXAMPLE).getObject();
    assertThat(dto).isEqualTo(PriceResponseMother.withAllProperties());
  }

  @Test
  public void shouldSerializeProperty() throws IOException {
    assertThat(this.json.write(PriceResponseMother.withAllProperties())).isEqualToJson(JSON_RESPONSE_EXAMPLE);
  }


}
