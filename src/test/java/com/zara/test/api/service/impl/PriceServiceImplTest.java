package com.zara.test.api.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.exception.NotFoundException;
import com.zara.test.api.mapper.PriceMapper;
import com.zara.test.api.objectmother.BrandMother;
import com.zara.test.api.objectmother.PriceMother;
import com.zara.test.api.objectmother.PriceResponseMother;
import com.zara.test.api.service.BrandService;
import java.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceImplTest {

  @InjectMocks
  private PriceServiceImpl priceService;

  @Mock
  private PriceMapper priceMapper;
  @Mock
  private BrandService brandService;

  @Test
  public void getPriceByBrand() {
    when(this.priceMapper.map(PriceMother.withAllProperties())).thenReturn(PriceResponseMother.withAllProperties());
    when(this.brandService.findById(1L)).thenReturn(BrandMother.withAllProperties());
    final PriceResponse priceResponse = this.priceService.getPriceByBrand(1L, 35455L, LocalDateTime.MIN);
    assertEquals(PriceResponseMother.withAllProperties(), priceResponse);
    verify(this.brandService, times(1)).findById(1L);
    verify(this.priceMapper, times(1)).map(PriceMother.withAllProperties());
  }


  @Test(expected = NotFoundException.class)
  public void notBrandFound() {
    when(this.brandService.findById(1L)).thenThrow(NotFoundException.class);
    this.priceService.getPriceByBrand(1L, 35455L, LocalDateTime.MIN);
  }

  @Test(expected = NotFoundException.class)
  public void notPriceFound() {
    when(this.brandService.findById(1L)).thenReturn(BrandMother.withAllProperties());
    this.priceService.getPriceByBrand(1L, 35455L, LocalDateTime.MAX);
  }
}
