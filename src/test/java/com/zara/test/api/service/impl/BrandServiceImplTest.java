package com.zara.test.api.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.zara.test.api.exception.NotFoundException;
import com.zara.test.api.objectmother.BrandMother;
import com.zara.test.api.repository.BrandRepository;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BrandServiceImplTest {

  @InjectMocks
  private BrandServiceImpl brandService;
  @Mock
  private BrandRepository repository;

  @Test
  public void save() {
    when(this.repository.save(BrandMother.withAllProperties())).thenReturn(BrandMother.withAllProperties());
    assertEquals(BrandMother.withAllProperties(), this.brandService.save(BrandMother.withAllProperties()));
    verify(this.repository, times(1)).save(BrandMother.withAllProperties());
  }

  @Test
  public void findById() {
    when(this.repository.findById(1L)).thenReturn(Optional.of(BrandMother.withAllProperties()));
    assertEquals(BrandMother.withAllProperties(), this.brandService.findById(1L));
    verify(this.repository, times(1)).findById(1L);
  }

  @Test(expected = NotFoundException.class)
  public void findPriceByBrandNotFound() {
    when(this.repository.findById(1L)).thenReturn(Optional.empty());
    this.brandService.findById(1L);
  }
}
