package com.zara.test.api.controller.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.service.PriceService;
import java.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

@RunWith(MockitoJUnitRunner.class)
public class PriceControllerImplTest {

  @InjectMocks
  private PriceControllerImpl priceController;

  @Mock
  private PriceService service;

  @Test
  public void getPriceByBrand() {

    Mockito.when(this.service.getPriceByBrand(1L, 35455L, LocalDateTime.MAX))
        .thenReturn(PriceResponse.builder().build());
    final ResponseEntity<PriceResponse> responseEntity = this.priceController
        .findPriceByBrand(1L, 35455L, LocalDateTime.MAX);
    verify(this.service, times(1)).getPriceByBrand(1L, 35455L, LocalDateTime.MAX);
    Assert.notNull(responseEntity.getBody(), "El objeto contiene valor");

  }
}
