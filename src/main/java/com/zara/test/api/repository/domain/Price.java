package com.zara.test.api.repository.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Table;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(appliesTo = "price")
public class Price implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @ManyToOne
  @JoinColumn(name = "BRAND_ID")
  private Brand brandId;
  @Column(name = "PRODUCT_ID")
  private Long productId;
  @Column(name = "START_DATE ")
  private Instant startDate;
  @Column(name = "PRICE_LIST")
  private Long priceList;
  @Column(name = "PRIORITY")
  private Integer priority;
  @Column(name = "PRICE")
  private BigDecimal value;
  @Column(name = "CURR")
  private String currency;

}
