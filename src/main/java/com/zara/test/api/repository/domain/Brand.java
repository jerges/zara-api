package com.zara.test.api.repository.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(appliesTo = "brand")
public class Brand implements Serializable {

  @Id
  @Column(name = "BRAND_ID", unique = true, nullable = false)
  private Long id;

  @Column(name = "BRAND_NAME")
  private String name;

  @OneToMany(cascade = CascadeType.ALL)
  private List<Price> priceList;

}
