package com.zara.test.api.exception;


public class NotFoundException extends RuntimeException {

  public NotFoundException(final String message) {
    super(message);
  }
}
