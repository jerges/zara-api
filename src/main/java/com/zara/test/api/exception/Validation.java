package com.zara.test.api.exception;

import java.io.Serializable;

public interface Validation extends Serializable {

  String getCode();

  String getMessage();

}
