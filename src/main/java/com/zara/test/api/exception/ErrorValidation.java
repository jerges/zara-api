package com.zara.test.api.exception;

public class ErrorValidation implements Validation {

  private final String code;
  private final String message;

  ErrorValidation(final String code, final String message) {
    this.code = code;
    this.message = message;
  }

  @Override
  public String getCode() {
    return this.code;
  }

  @Override
  public String getMessage() {
    return this.message;
  }
}
