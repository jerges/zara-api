package com.zara.test.api.controller.impl;

import com.zara.test.api.config.SwaggerConfiguration;
import com.zara.test.api.controller.PriceController;
import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.service.PriceService;
import java.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = SwaggerConfiguration.API_BASE_PATH)
public class PriceControllerImpl implements PriceController {

  private final PriceService service;

  public PriceControllerImpl(final PriceService service) {
    this.service = service;
  }

  @Override
  @GetMapping("{brandId}/{productId}")
  public ResponseEntity<PriceResponse> findPriceByBrand(@PathVariable final Long brandId,
      @PathVariable final Long productId,
      @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime startDate) {
    return ResponseEntity.ok(this.service.getPriceByBrand(brandId, productId, startDate));
  }
}
