package com.zara.test.api.controller;

import com.zara.test.api.config.SwaggerConfiguration;
import com.zara.test.api.dto.PriceResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

@Api(value = SwaggerConfiguration.API_TITLE)
@Validated
public interface PriceController {

  ResponseEntity<PriceResponse> findPriceByBrand(
      @ApiParam(value = "Nombre de la marca del producto ", required = true, example = "ZARA")
      @NotBlank(message = "el nombre es obligatorio") final Long brandId,
      @ApiParam(value = "codigo del producto", required = true, example = "35455")
      @NotBlank(message = "el codigo es obligatorio") final Long productId,
      @ApiParam(value = "Fecha de aplicación del precio ", required = true, example = "2020-06-15")
      @NotBlank(message = "la fecha es obligatoria") final LocalDateTime startDate);

}
