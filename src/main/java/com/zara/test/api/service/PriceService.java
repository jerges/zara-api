package com.zara.test.api.service;

import com.zara.test.api.dto.PriceResponse;
import java.time.LocalDateTime;

public interface PriceService {


  PriceResponse getPriceByBrand(final Long brandId, final Long productId, final LocalDateTime startDate);


}
