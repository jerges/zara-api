package com.zara.test.api.service.impl;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.exception.NotFoundException;
import com.zara.test.api.mapper.PriceMapper;
import com.zara.test.api.repository.domain.Brand;
import com.zara.test.api.service.BrandService;
import com.zara.test.api.service.PriceService;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PriceServiceImpl implements PriceService {

  private final PriceMapper priceMapper;
  private final BrandService service;

  public PriceServiceImpl(final PriceMapper priceMapper, final BrandService service) {
    this.priceMapper = priceMapper;
    this.service = service;
  }

  @Override
  public PriceResponse getPriceByBrand(final Long brandId, final Long productId, final LocalDateTime startDate) {

    final Brand brand = this.service.findById(brandId);
    return brand.getPriceList()
        .stream()
        .filter(price -> price.getProductId().equals(productId) && price.getStartDate()
            .equals(startDate.toInstant(ZoneOffset.UTC)))
        .map(this.priceMapper::map)
        .findFirst().orElseThrow(() -> new NotFoundException("El precio no existe para ese criterio de busqueda"));
  }
}
