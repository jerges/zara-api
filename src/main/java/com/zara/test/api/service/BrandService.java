package com.zara.test.api.service;

import com.zara.test.api.repository.domain.Brand;

public interface BrandService {

  Brand save(Brand brand);


  Brand findById(Long id);

}
