package com.zara.test.api.service.impl;

import com.zara.test.api.exception.NotFoundException;
import com.zara.test.api.repository.BrandRepository;
import com.zara.test.api.repository.domain.Brand;
import com.zara.test.api.service.BrandService;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

  private final BrandRepository repository;

  public BrandServiceImpl(final BrandRepository repository) {
    this.repository = repository;
  }

  @Override
  public Brand save(final Brand brand) {
    return this.repository.save(brand);
  }

  @Override
  public Brand findById(final Long id) {
    return this.repository.findById(id).orElseThrow(() -> new NotFoundException("El comercio no existe"));
  }
}
