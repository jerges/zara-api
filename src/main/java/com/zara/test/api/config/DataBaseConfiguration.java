package com.zara.test.api.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EntityScan("com.zara.test.api.repository.domain")
@EnableJpaRepositories(basePackages = "com.zara.test.api.repository")
public class DataBaseConfiguration {

}


