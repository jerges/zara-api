package com.zara.test.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The type Swagger configuration.
 * <p>
 * Configuration Spring Fox Swagger for Data Api
 */
@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

  public static final String API_VERSION = "1.0.0";
  public static final String API_BASE_PATH = "/price";
  public static final String API_TITLE = "zara_test_api";
  private static final String API_DESCRIPTION = "Test for Inditex";


  @Bean("SwaggerEntityDataApiDocket")
  public Docket swaggerSpringMvcPlugin() {
    return new Docket(DocumentationType.SWAGGER_2)
        .forCodeGeneration(true)
        .useDefaultResponseMessages(false)
        .directModelSubstitute(Object.class, Void.class)
        .apiInfo(this.apiInfo())
        .select()
        .build();
  }


  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title(API_TITLE)
        .description(API_DESCRIPTION)
        .version(API_VERSION)
        .build();
  }

}
