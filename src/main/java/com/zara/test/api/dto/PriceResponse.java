package com.zara.test.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class PriceResponse implements Serializable {

  @JsonPropertyDescription("Identificador del producto.")
  @ApiModelProperty("Identificador del producto.")
  @JsonProperty("product_id")
  Long productId;
  @JsonPropertyDescription("Identificador de la cadena.")
  @ApiModelProperty("Identificador de la cadena.")
  @JsonProperty("brand_id")
  Long brandId;
  @JsonPropertyDescription("tarifa a aplicar.")
  @ApiModelProperty("tarifa a aplicar.")
  @JsonProperty("price_list")
  Long priceList;
  @JsonPropertyDescription("fechas de aplicación .")
  @ApiModelProperty("fechas de aplicación .")
  @JsonProperty("start_date")
  LocalDateTime startDate;
  @JsonPropertyDescription("precio final a aplicar.")
  @ApiModelProperty("precio final a aplicar.")
  @JsonProperty("price")
  BigDecimal price;

}
