package com.zara.test.api.mapper;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.mapper.impl.PriceMapperDecorator;
import com.zara.test.api.repository.domain.Price;
import org.mapstruct.DecoratedWith;

@DecoratedWith(PriceMapperDecorator.class)
public interface PriceMapper {

  PriceResponse map(Price price);

}
