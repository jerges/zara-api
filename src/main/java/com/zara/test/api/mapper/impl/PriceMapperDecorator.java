package com.zara.test.api.mapper.impl;

import com.zara.test.api.dto.PriceResponse;
import com.zara.test.api.mapper.PriceMapper;
import com.zara.test.api.repository.domain.Price;
import java.time.LocalDateTime;
import java.time.ZoneId;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class PriceMapperDecorator implements PriceMapper {

  @Override
  public PriceResponse map(final Price price) {
    return PriceResponse.builder()
        .price(price.getValue())
        .brandId(price.getBrandId() != null ? price.getBrandId().getId() : null)
        .priceList(price.getPriceList())
        .productId(price.getProductId())
        .startDate(LocalDateTime.ofInstant(price.getStartDate(), ZoneId.of("UTC")))
        .build();
  }
}
